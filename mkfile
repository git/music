all:V: install

PREFIX=`echo ${PREFIX:-/usr/local}`
install:V: music play local yt file stream #playlist
	mkdir -m755 -p $PREFIX/bin
	cp music $PREFIX/bin/music
	chmod 755 $PREFIX/bin/music
	cp play $PREFIX/bin/play
	chmod 755 $PREFIX/bin/play
	ln -s $PREFIX/bin/play $PREFIX/bin/queue
	mkdir -m755 -p $PREFIX/share/play
	cp -R local $PREFIX/share/play/local
	chmod a+rX,u+rwX $PREFIX/share/play/local
	cp -R yt $PREFIX/share/play/file
	chmod a+rX,u+rwX $PREFIX/share/play/file
	cp -R yt $PREFIX/share/play/yt
	chmod a+rX,u+rwX $PREFIX/share/play/yt
	cp -R yt $PREFIX/share/play/stream
	chmod a+rX,u+rwX $PREFIX/share/play/stream

uninstall:V:
	rm $PREFIX/bin/music
	rm $PREFIX/bin/play
	rm $PREFIX/bin/queue
	rm -r $PREFIX/share/play
